"""
NetIE.

This file is part of NetIE.

NetIE ("Network Include / Exclude manager") is a little command-line-interface (cli) tool that has
been thought to help you configure the WireGuard's `AllowedIPs` field, e.g. like described here:
https://web.archive.org/web/20210705213722/https://www.procustodibus.com/blog/2021/03/wireguard-allowedips-calculator/

Copyright ©  2021  Stéphane Tzvetkov

NetIE is free software: you can redistribute it and/or modify it under the terms of the GNU Affero
General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

NetIE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along with NetIE. If not,
see <https://www.gnu.org/licenses/>.
"""

__version__ = "1.0.0"

from ipaddress import (
    _BaseNetwork as BaseNetwork,
    IPv4Address,
    IPv4Network,
    IPv6Address,
    IPv6Network,
    ip_address,
    ip_network,
)
import itertools
import enum
import click


class IPV(enum.Enum):
    """
    IPv4 or IPv6 enum.
    """

    IPV4 = 1
    IPV6 = 2


class IE(enum.Enum):
    """
    `-i / --include` or `-e / --exclude` arg enum.
    """

    INCLUDE = 1
    EXCLUDE = 2


class NetwAddr(enum.Enum):
    """
    Address or network enum.
    """

    ADDRESS = 1
    NETWORK = 2


@click.command()
@click.option(
    "-i",
    "--include",
    "include",
    help="A comma separated list of IPv4/6 addresses (and/or IPv4/6 networks) to be included.",
)
@click.option(
    "-e",
    "--exclude",
    "exclude",
    help="A comma separated list of IPv4/6 addresses (and/or IPv4/6 networks) to be excluded.",
)
@click.version_option(version=__version__, message="NetIE version %(version)s")
def main(include: str, exclude: str) -> None:
    r"""
    NetIE ("Network Include / Exclude manager") is a little command-line-interface (cli) tool that
    has been thought to help you configure the WireGuard's `AllowedIPs` field, e.g. like described
    here:
    https://web.archive.org/web/20210705213722/https://www.procustodibus.com/blog/2021/03/wireguard-allowedips-calculator/

    Copyright ©  2021  Stéphane Tzvetkov

    This program is free software: you can redistribute it and/or modify it under the terms of the
    GNU Affero General Public License as published by the Free Software Foundation, either version
    3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
    the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with this
    program. If not, see <https://www.gnu.org/licenses/>.

    \f
    Main function / entry point of NetIE.

    :param include: str: IP(s) to include
    :param exclude: str: IP(s) to exclude
    """
    # makes sure that -i/--include and -e/--exclude args are not empty
    check_args(include, exclude)

    # extracts IPs list from arguments
    (ipv4_include_str, ipv6_include_str) = extract_ipv4s_and_ipv6s(include, IE.INCLUDE)
    (ipv4_exclude_str, ipv6_exclude_str) = extract_ipv4s_and_ipv6s(exclude, IE.EXCLUDE)

    # checks IPs validity
    check_ips_from_list(ipv4_include_str, IPV.IPV4, IE.INCLUDE)
    check_ips_from_list(ipv4_exclude_str, IPV.IPV4, IE.EXCLUDE)
    check_ips_from_list(ipv6_include_str, IPV.IPV6, IE.INCLUDE)
    check_ips_from_list(ipv6_exclude_str, IPV.IPV6, IE.EXCLUDE)

    # convert str IPs to BaseNetwork IPs
    ipv4_include: list[BaseNetwork] = convert_ips(ipv4_include_str)
    ipv6_include: list[BaseNetwork] = convert_ips(ipv6_include_str)
    ipv4_exclude: list[BaseNetwork] = convert_ips(ipv4_exclude_str)
    ipv6_exclude: list[BaseNetwork] = convert_ips(ipv6_exclude_str)

    # checks that two IPs, from the same inclusion or exlusion list, are not overlaping
    check_no_ip_overlaps(ipv4_include, IE.INCLUDE)
    check_no_ip_overlaps(ipv6_include, IE.INCLUDE)
    check_no_ip_overlaps(ipv4_exclude, IE.EXCLUDE)
    check_no_ip_overlaps(ipv6_exclude, IE.EXCLUDE)

    # checks that the same IP isn't present in the inclusion and exclusion list at the same time
    check_no_same_ip_inc_exc(ipv4_include, ipv4_exclude)
    check_no_same_ip_inc_exc(ipv6_include, ipv6_exclude)

    # resolves overlaps between the IPv4 inclusion list and exclusion list
    ipv4_results = resolve_overlapping_lists(include=ipv4_include, exclude=ipv4_exclude)
    sorted(ipv4_results)
    ipv4_results.reverse()

    # resolves overlaps between the IPv6 inclusion list and exclusion list
    ipv6_results = resolve_overlapping_lists(include=ipv6_include, exclude=ipv6_exclude)
    sorted(ipv6_results)
    ipv6_results.reverse()

    # prints the results
    ipv4_str_results: str = ", ".join(str(x) for x in ipv4_results)
    ipv6_str_results: str = ", ".join(str(x) for x in ipv6_results)
    if ipv4_str_results and ipv6_str_results:
        print(ipv4_str_results + ", " + ipv6_str_results)
    if ipv4_str_results and not ipv6_str_results:
        print(ipv4_str_results)
    if not ipv4_str_results and ipv6_str_results:
        print(ipv6_str_results)


def check_args(include: str, exclude: str) -> None:
    """
    Make sure that the inclusion list and the exclusion list are not empty.

    :param include: str: IP(s) to include
    :param exclude: str: IP(s) to exclude
    :raises exception: click: thrown if the inclusion list or the exclusion list is empty
    """
    if not include:
        click.secho(
            "[Error] - The inclusion list (the one passed with the `-i` or `--include` option) "
            "is empty. Please add some IP adresses and/or networks to this list and try again.",
            fg="red",
        )
        raise click.Abort()
    if not exclude:
        click.secho(
            "[Error] - The excludion list (the one passed with the `-e` or `--exclude` option) "
            "is empty. Please add some IP adresses and/or networks to this list and try again.",
            fg="red",
        )
        raise click.Abort()


def extract_ipv4s_and_ipv6s(ips_str: str, i_e: IE) -> tuple[list[str], list[str]]:
    """
    Separate IPv4 in one list, and IPv6 in another list.

    :param ips_str: str: raw character string of IP addresse(s) and/or IP network(s)
    :param i_e: IE: IP origin (from inclusion list or exclusion list)
    :raises exception: click: thrown if an IP does not contains a '.' (if IPv4) nor a ':' (if IPv6)
    :returns: tuple[list[str], list[str]]: a list containing IPv4s and another containing IPv6s
    """
    ips: list[str] = list(ips_str.replace(" ", "").split(","))  # convert from string to list
    ips = list(set(ips))  # remove duplicates
    ipv4: list[str] = []
    ipv6: list[str] = []
    for ip_item in ips:
        if "." in ip_item:
            ipv4.append(ip_item)
        elif ":" in ip_item:
            ipv6.append(ip_item)
        else:
            ie_str = "inclusion" if i_e == IE.INCLUDE else "exclusion"
            ie_opt_str = "-i" if i_e == IE.INCLUDE else "-e"
            ie_option_str = "--include" if i_e == IE.INCLUDE else "--exclude"
            click.secho(
                f"[Error] - The following character string, from the {ie_str} list (the one passed "
                f"with the `{ie_opt_str}` or `{ie_option_str}` option), isn't formated like an IP "
                f"address and/or network:\n"
                f"          {ip_item}\n\n"
                f"Please fix your {ie_str} list and try again.",
                fg="red",
            )
            raise click.Abort()
    return (ipv4, ipv6)


def check_ips_from_list(ips: list[str], ipv: IPV, i_e: IE) -> None:
    """
    Make sure that a list of IPs is valid.

    :param ips: list[str]: IPs to check
    :param ipv: IPV: IP type (IPv4 or IPv6)
    :param i_e: IE: IP origin (from inclusion list or exclusion list)
    """
    for ip_str in ips:
        if "/" in ip_str:
            check_ip(ip_str, ipv, i_e, NetwAddr.NETWORK)
        else:
            check_ip(ip_str, ipv, i_e, NetwAddr.ADDRESS)


def check_ip(ip_str: str, ipv: IPV, i_e: IE, netwaddr: NetwAddr) -> None:
    """
    Make sure that an IP is valid.

    :param ip_str: str: IP to check
    :param ipv: IPV: IP type (IPv4 or IPv6)
    :param i_e: IE: IP origin (from inclusion list or exclusion list)
    :param netwaddr: NetwAddr: IP address or IP network
    :raises exception: click: thrown if an IP isn't valid
    """
    try:
        if (
            ipv == IPV.IPV4
            and netwaddr == NetwAddr.ADDRESS
            and isinstance(ip_address(ip_str), IPv4Address)
        ):
            pass
        elif (
            ipv == IPV.IPV4
            and netwaddr == NetwAddr.NETWORK
            and isinstance(ip_network(ip_str), IPv4Network)
        ):
            pass
        elif (
            ipv == IPV.IPV6
            and netwaddr == NetwAddr.ADDRESS
            and isinstance(ip_address(ip_str), IPv6Address)
        ):
            pass
        elif (
            ipv == IPV.IPV6
            and netwaddr == NetwAddr.NETWORK
            and isinstance(ip_network(ip_str), IPv6Network)
        ):
            pass
    except ValueError as exc:
        ie_str = "inclusion" if i_e == IE.INCLUDE else "exclusion"
        ie_opt_str = "-i" if i_e == IE.INCLUDE else "-e"
        ie_option_str = "--include" if i_e == IE.INCLUDE else "--exclude"
        if "has host bits set" in str(exc):
            click.secho(
                f"[Error] - The following IP network from the {ie_str} list (the one passed with "
                f"the `{ie_opt_str}` or `{ie_option_str}` option), has the hosts bits set:\n"
                f"          {ip_str}\n\n"
                f"Please fix your {ie_str} list and try again.",
                fg="red",
            )
        else:
            click.secho(
                f"[Error] - The following IP, from the {ie_str} list (the one passed with the "
                f"`{ie_opt_str}` or `{ie_option_str}` option), isn't valid:\n"
                f"          {exc}\n\n"
                f"Please fix your {ie_str} list and try again.",
                fg="red",
            )
        raise click.Abort()


def convert_ips(ips: list[str]) -> list[BaseNetwork]:
    """
    Convert IPs from a list of String to a list of BaseNetwork.

    :param ips: list[str]: IPs to convert
    :returns list[BaseNetwork]: converted IPs
    """
    netw_ips: list[BaseNetwork] = []
    for ip_str in ips:
        if isinstance(ip_network(ip_str), BaseNetwork or IPv4Network or IPv6Network):
            netw_ips.append(ip_network(ip_str))
    return netw_ips


def check_no_ip_overlaps(ips: list[BaseNetwork], i_e: IE) -> None:
    """
    Make sure that the IP addresse(s) and/or IP network(s) passed in argument (`ips` variable) are
    not overlaping.

    :param ips: list[BaseNetwork]: IP addresse(s) and/or IP network(s)
    :param i_e: IE: IP origin (from inclusion list or exclusion list)
    :raises exception: click: thrown if IPs are overlaping
    """
    for ip_combination in itertools.combinations(ips, 2):
        ip0: BaseNetwork = ip_combination[0]
        ip1: BaseNetwork = ip_combination[1]
        if ip0.overlaps(ip1):
            ie_str = "inclusion" if i_e == IE.INCLUDE else "exclusion"
            ie_opt_str = "-i" if i_e == IE.INCLUDE else "-e"
            ie_option_str = "--include" if i_e == IE.INCLUDE else "--exclude"
            click.secho(
                f"[Error] - The following IP address(es), and/or IP network(s), are overlaping in "
                f"the {ie_str} list (the one passed with the `{ie_opt_str}` or `{ie_option_str}` "
                f"option):\n"
                f"          {str(ip0)}\n"
                f"          {str(ip1)}\n\n"
                f"Please fix your {ie_str} list and try again.\n",
                fg="red",
            )
            raise click.Abort()


def check_no_same_ip_inc_exc(include: list[BaseNetwork], exclude: list[BaseNetwork]) -> None:
    """
    Make sure that an IPv4 address and/or an IPv4 network is not present in the inclusion list
    (`include` variable) and the exclusion list (`exclude`variable) at the same time.

    :param include: list[BaseNetwork]: IP addresse(s) and/or IP network(s) from the inclusion list
    :param exclude: list[BaseNetwork]: IP addresse(s) and/or IP network(s) from the exclusion list
    :raises exception: click: thrown if an IP is present in the inclusion and exclusion list
    """
    for inc in include:
        for exc in exclude:
            if inc == exc:
                click.secho(
                    f"[Error] - The following IP address, and/or IP network, is present in the "
                    f"inclusion list (the one passed with the `-i` or `--include` option) and the "
                    f"exclusion list (the one passed with the `-e` or `--exclude` option) at the "
                    f"same time:\n"
                    f"          {str(inc)}\n\n"
                    f"Please fix your inclusion and exclusion lists and try again.\n",
                    fg="red",
                )
                raise click.Abort()


def resolve_overlapping_lists(
    include: list[BaseNetwork], exclude: list[BaseNetwork]
) -> list[BaseNetwork]:
    """
    Resolve overlaps between the inclusion list (`-i` or `--include` argument) and the exclusion
    list (`-e` or `--exclude` argument).

    :param include: list[BaseNetwork]: IP addresse(s) and/or IP network(s) from the inclusion list
    :param exclude: list[BaseNetwork]: IP addresse(s) and/or IP network(s) from the exclusion list
    :returns: list[BaseNetwork]: IP addresse(s) and/or IP network(s) resolving the overlaps
    """
    for exc in exclude:
        for inc in include.copy():
            if inc.overlaps(exc):
                resolve_overlapping_ips(include, inc, exc)
    return include


def resolve_overlapping_ips(include: list[BaseNetwork], inc: BaseNetwork, exc: BaseNetwork) -> None:
    """
    Add and/or remove IPs from the include list (future results list) in order to resolves
    overlaps.

    :param include: list[BaseNetwork]: IP address(es) and/or IP network(s) from the inclusion list
    :param inc: BaseNetwork: included IP address(es) and/or IP network(s) to check
    :param exc: BaseNetwork: excluded IP address(es) and/or IP network(s) to check
    """
    if inc == exc:
        include.remove(inc)
    else:
        for res in inc.address_exclude(exc):
            if res not in include:
                include.append(res)
        if inc in include:
            include.remove(inc)


if __name__ == "__main__":  # pragma: no cover
    main()  # pylint: disable=no-value-for-parameter
