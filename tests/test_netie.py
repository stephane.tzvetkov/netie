"""
...
"""

from click.testing import CliRunner

from netie.netie import __version__  # type: ignore
from netie.netie import main  # type: ignore

runner = CliRunner()


def test_version() -> None:
    """
    Test netie version
    """
    assert __version__ == "1.0.0"


def test_help_and_version() -> None:
    """
    Test directly the `--help` and `--version` options.
    """
    result = runner.invoke(main, ["--help"])
    # print(result.output)
    assert result.exit_code == 0

    result = runner.invoke(main, ["--version"])
    # print(result.output)
    assert result.exit_code == 0


def test_include_all_ipv4_and_exclude_one_ipv4() -> None:
    """
    Test netie when including `0.0.0.0/0` (all IPv4) and excluding `10.0.0.1`.
    """
    result = runner.invoke(main, ["--include", "0.0.0.0/0", "--exclude", "10.0.0.1"])
    # print(result.output)
    assert result.exit_code == 0
    results: set[str] = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "0.0.0.0/5",
            "8.0.0.0/7",
            "10.0.0.0/32",
            "10.0.0.2/31",
            "10.0.0.4/30",
            "10.0.0.8/29",
            "10.0.0.16/28",
            "10.0.0.32/27",
            "10.0.0.64/26",
            "10.0.0.128/25",
            "10.0.1.0/24",
            "10.0.2.0/23",
            "10.0.4.0/22",
            "10.0.8.0/21",
            "10.0.16.0/20",
            "10.0.32.0/19",
            "10.0.64.0/18",
            "10.0.128.0/17",
            "10.1.0.0/16",
            "10.2.0.0/15",
            "10.4.0.0/14",
            "10.8.0.0/13",
            "10.16.0.0/12",
            "10.32.0.0/11",
            "10.64.0.0/10",
            "10.128.0.0/9",
            "11.0.0.0/8",
            "12.0.0.0/6",
            "16.0.0.0/4",
            "32.0.0.0/3",
            "64.0.0.0/2",
            "128.0.0.0/1",
        ]
    )


def test_include_ipv4_network_and_exclude_one_ipv4_inside_that_network() -> None:
    """
    Test netie when including `10.0.0.0/8` (IPv4 network) and excluding `10.0.0.1` (inside the
    network).
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8", "--exclude", "10.0.0.1"])
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "10.0.0.0/32",
            "10.0.0.2/31",
            "10.0.0.4/30",
            "10.0.0.8/29",
            "10.0.0.16/28",
            "10.0.0.32/27",
            "10.0.0.64/26",
            "10.0.0.128/25",
            "10.0.1.0/24",
            "10.0.2.0/23",
            "10.0.4.0/22",
            "10.0.8.0/21",
            "10.0.16.0/20",
            "10.0.32.0/19",
            "10.0.64.0/18",
            "10.0.128.0/17",
            "10.1.0.0/16",
            "10.2.0.0/15",
            "10.4.0.0/14",
            "10.8.0.0/13",
            "10.16.0.0/12",
            "10.32.0.0/11",
            "10.64.0.0/10",
            "10.128.0.0/9",
        ]
    )


def test_include_ipv4_network_and_exclude_two_ipv4_inside_that_network() -> None:
    """
    Test netie when including `10.0.0.0/8` (IPv4 network) and excluding `10.0.0.0, 10.0.0.1`
    (inside the network).
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8", "--exclude", "10.0.0.0, 10.0.0.1"])
    print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "10.0.0.2/31",
            "10.0.0.4/30",
            "10.0.0.8/29",
            "10.0.0.16/28",
            "10.0.0.32/27",
            "10.0.0.64/26",
            "10.0.0.128/25",
            "10.0.1.0/24",
            "10.0.2.0/23",
            "10.0.4.0/22",
            "10.0.8.0/21",
            "10.0.16.0/20",
            "10.0.32.0/19",
            "10.0.64.0/18",
            "10.0.128.0/17",
            "10.1.0.0/16",
            "10.2.0.0/15",
            "10.4.0.0/14",
            "10.8.0.0/13",
            "10.16.0.0/12",
            "10.32.0.0/11",
            "10.64.0.0/10",
            "10.128.0.0/9",
        ]
    )


def test_include_ipv4_network_and_exclude_one_ipv4_outside_that_network() -> None:
    """
    Test netie when including `10.0.0.0/8` (IPv4 network) and excluding `42.0.0.1` (outside the
    network).
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8", "--exclude", "42.0.0.1"])
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(["10.0.0.0/8"])


def test_include_all_ipv6_and_exclude_one_ipv6() -> None:
    """
    Test netie when including `::/0` (all IPv6) and excluding
    `0123:4567:89ab:cdef:0123:4567:89ab:cdef`.
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "::/0",
            "--exclude",
            "0123:4567:89ab:cdef:0123:4567:89ab:cdef",
        ],
    )
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "::/8",
            "100::/11",
            "120::/15",
            "122::/16",
            "123::/18",
            "123:4000::/22",
            "123:4400::/24",
            "123:4500::/26",
            "123:4540::/27",
            "123:4560::/30",
            "123:4564::/31",
            "123:4566::/32",
            "123:4567::/33",
            "123:4567:8000::/37",
            "123:4567:8800::/40",
            "123:4567:8900::/41",
            "123:4567:8980::/43",
            "123:4567:89a0::/45",
            "123:4567:89a8::/47",
            "123:4567:89aa::/48",
            "123:4567:89ab::/49",
            "123:4567:89ab:8000::/50",
            "123:4567:89ab:c000::/53",
            "123:4567:89ab:c800::/54",
            "123:4567:89ab:cc00::/56",
            "123:4567:89ab:cd00::/57",
            "123:4567:89ab:cd80::/58",
            "123:4567:89ab:cdc0::/59",
            "123:4567:89ab:cde0::/61",
            "123:4567:89ab:cde8::/62",
            "123:4567:89ab:cdec::/63",
            "123:4567:89ab:cdee::/64",
            "123:4567:89ab:cdef::/72",
            "123:4567:89ab:cdef:100::/75",
            "123:4567:89ab:cdef:120::/79",
            "123:4567:89ab:cdef:122::/80",
            "123:4567:89ab:cdef:123::/82",
            "123:4567:89ab:cdef:123:4000::/86",
            "123:4567:89ab:cdef:123:4400::/88",
            "123:4567:89ab:cdef:123:4500::/90",
            "123:4567:89ab:cdef:123:4540::/91",
            "123:4567:89ab:cdef:123:4560::/94",
            "123:4567:89ab:cdef:123:4564::/95",
            "123:4567:89ab:cdef:123:4566::/96",
            "123:4567:89ab:cdef:123:4567::/97",
            "123:4567:89ab:cdef:123:4567:8000:0/101",
            "123:4567:89ab:cdef:123:4567:8800:0/104",
            "123:4567:89ab:cdef:123:4567:8900:0/105",
            "123:4567:89ab:cdef:123:4567:8980:0/107",
            "123:4567:89ab:cdef:123:4567:89a0:0/109",
            "123:4567:89ab:cdef:123:4567:89a8:0/111",
            "123:4567:89ab:cdef:123:4567:89aa:0/112",
            "123:4567:89ab:cdef:123:4567:89ab:0/113",
            "123:4567:89ab:cdef:123:4567:89ab:8000/114",
            "123:4567:89ab:cdef:123:4567:89ab:c000/117",
            "123:4567:89ab:cdef:123:4567:89ab:c800/118",
            "123:4567:89ab:cdef:123:4567:89ab:cc00/120",
            "123:4567:89ab:cdef:123:4567:89ab:cd00/121",
            "123:4567:89ab:cdef:123:4567:89ab:cd80/122",
            "123:4567:89ab:cdef:123:4567:89ab:cdc0/123",
            "123:4567:89ab:cdef:123:4567:89ab:cde0/125",
            "123:4567:89ab:cdef:123:4567:89ab:cde8/126",
            "123:4567:89ab:cdef:123:4567:89ab:cdec/127",
            "123:4567:89ab:cdef:123:4567:89ab:cdee/128",
            "123:4567:89ab:cdef:123:4567:89ab:cdf0/124",
            "123:4567:89ab:cdef:123:4567:89ab:ce00/119",
            "123:4567:89ab:cdef:123:4567:89ab:d000/116",
            "123:4567:89ab:cdef:123:4567:89ab:e000/115",
            "123:4567:89ab:cdef:123:4567:89ac:0/110",
            "123:4567:89ab:cdef:123:4567:89b0:0/108",
            "123:4567:89ab:cdef:123:4567:89c0:0/106",
            "123:4567:89ab:cdef:123:4567:8a00:0/103",
            "123:4567:89ab:cdef:123:4567:8c00:0/102",
            "123:4567:89ab:cdef:123:4567:9000:0/100",
            "123:4567:89ab:cdef:123:4567:a000:0/99",
            "123:4567:89ab:cdef:123:4567:c000:0/98",
            "123:4567:89ab:cdef:123:4568::/93",
            "123:4567:89ab:cdef:123:4570::/92",
            "123:4567:89ab:cdef:123:4580::/89",
            "123:4567:89ab:cdef:123:4600::/87",
            "123:4567:89ab:cdef:123:4800::/85",
            "123:4567:89ab:cdef:123:5000::/84",
            "123:4567:89ab:cdef:123:6000::/83",
            "123:4567:89ab:cdef:123:8000::/81",
            "123:4567:89ab:cdef:124::/78",
            "123:4567:89ab:cdef:128::/77",
            "123:4567:89ab:cdef:130::/76",
            "123:4567:89ab:cdef:140::/74",
            "123:4567:89ab:cdef:180::/73",
            "123:4567:89ab:cdef:200::/71",
            "123:4567:89ab:cdef:400::/70",
            "123:4567:89ab:cdef:800::/69",
            "123:4567:89ab:cdef:1000::/68",
            "123:4567:89ab:cdef:2000::/67",
            "123:4567:89ab:cdef:4000::/66",
            "123:4567:89ab:cdef:8000::/65",
            "123:4567:89ab:cdf0::/60",
            "123:4567:89ab:ce00::/55",
            "123:4567:89ab:d000::/52",
            "123:4567:89ab:e000::/51",
            "123:4567:89ac::/46",
            "123:4567:89b0::/44",
            "123:4567:89c0::/42",
            "123:4567:8a00::/39",
            "123:4567:8c00::/38",
            "123:4567:9000::/36",
            "123:4567:a000::/35",
            "123:4567:c000::/34",
            "123:4568::/29",
            "123:4570::/28",
            "123:4580::/25",
            "123:4600::/23",
            "123:4800::/21",
            "123:5000::/20",
            "123:6000::/19",
            "123:8000::/17",
            "124::/14",
            "128::/13",
            "130::/12",
            "140::/10",
            "180::/9",
            "200::/7",
            "400::/6",
            "800::/5",
            "1000::/4",
            "2000::/3",
            "4000::/2",
            "8000::/1",
        ]
    )


def test_include_ipv6_network_and_exclude_one_ipv6_inside_that_network() -> None:
    """
    Test netie when including `0123:4567:89ab:cdef:0123:4567:89ab:0/112` (IPv6 network) and
    excluding `0123:4567:89ab:cdef:0123:4567:89ab:cdef` (inside the network).
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "0123:4567:89ab:cdef:0123:4567:89ab:0/112",
            "--exclude",
            "0123:4567:89ab:cdef:0123:4567:89ab:cdef",
        ],
    )
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "123:4567:89ab:cdef:123:4567:89ab:0/113",
            "123:4567:89ab:cdef:123:4567:89ab:8000/114",
            "123:4567:89ab:cdef:123:4567:89ab:c000/117",
            "123:4567:89ab:cdef:123:4567:89ab:c800/118",
            "123:4567:89ab:cdef:123:4567:89ab:cc00/120",
            "123:4567:89ab:cdef:123:4567:89ab:cd00/121",
            "123:4567:89ab:cdef:123:4567:89ab:cd80/122",
            "123:4567:89ab:cdef:123:4567:89ab:cdc0/123",
            "123:4567:89ab:cdef:123:4567:89ab:cde0/125",
            "123:4567:89ab:cdef:123:4567:89ab:cde8/126",
            "123:4567:89ab:cdef:123:4567:89ab:cdec/127",
            "123:4567:89ab:cdef:123:4567:89ab:cdee/128",
            "123:4567:89ab:cdef:123:4567:89ab:cdf0/124",
            "123:4567:89ab:cdef:123:4567:89ab:ce00/119",
            "123:4567:89ab:cdef:123:4567:89ab:d000/116",
            "123:4567:89ab:cdef:123:4567:89ab:e000/115",
        ]
    )


def test_include_ipv6_network_and_exclude_one_ipv6_outside_that_network() -> None:
    """
    Test netie when including `0123:4567:89ab:cdef:0123:4567:89ab:0/112` (IPv6 network) and
    excluding `321:0001:0001:0001:0001:0001:0001:0001` (outside the network).
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "0123:4567:89ab:cdef:0123:4567:89ab:0/112",
            "--exclude",
            "321:0001:0001:0001:0001:0001:0001:0001",
        ],
    )
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "123:4567:89ab:cdef:123:4567:89ab:0/112",
        ]
    )


def test_include_ipv4_6_networks_and_exclude_ipv4_6_outside_those_networks() -> None:
    """
    Test netie when including `0123:4567:89ab:cdef:0123:4567:89ab:0/112` (IPv6 network) and
    excluding `321:0001:0001:0001:0001:0001:0001:0001` (outside the network).
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "10.0.0.0/8, 0123:4567:89ab:cdef:0123:4567:89ab:0/112",
            "--exclude",
            "42.0.0.1, 321:0001:0001:0001:0001:0001:0001:0001",
        ],
    )
    # print(result.output)
    assert result.exit_code == 0
    results = set(result.output.replace("\n", "").split(", "))
    assert results == set(
        [
            "10.0.0.0/8",
            "123:4567:89ab:cdef:123:4567:89ab:0/112",
        ]
    )


def test_empty_include_error() -> None:
    """
    Test netie with an empty `-i` or `--include` arg.
    """
    result = runner.invoke(main, ["--include", "", "--exclude", "10.0.0.1"])
    # print(result.output)
    assert result.exit_code == 1


def test_empty_exclude_error() -> None:
    """
    Test netie with an empty `-e` or `--exclude` arg.
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8", "--exclude", ""])
    # print(result.output)
    assert result.exit_code == 1


def test_bad_ipv4_format_error() -> None:
    """
    Test netie with the following poorly formated IP `10.0.0.999`.
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8", "--exclude", "10.0.0.999"])
    # print(result.output)
    assert result.exit_code == 1


def test_bad_ipv6_format_error() -> None:
    """
    Test netie with the following poorly formated IP `10.0.0.999`.
    """
    result = runner.invoke(
        main, ["--include", "::/0", "--exclude", "0123:4567:89ab:cdef:0123:4567:89ab:gggg"]
    )
    # print(result.output)
    assert result.exit_code == 1


def test_invalid_ip_format_error() -> None:
    """
    Test netie with the following invalid formated IP `plop`.
    """
    result = runner.invoke(main, ["--include", "0.0.0.0/0, ::/0", "--exclude", "plop"])
    # print(result.output)
    assert result.exit_code == 1


def test_ipv4_host_bit_set_error() -> None:
    """
    Test netie when including `10.1.1.1/8` (where host bits are set) and excluding `10.0.0.1`.
    """
    result = runner.invoke(main, ["--include", "10.1.1.1/8", "--exclude", "10.0.0.1"])
    # print(result.output)
    assert result.exit_code == 1


def test_ipv6_host_bit_set_error() -> None:
    """
    Test netie when including `0123:0001:0001:0001:0001:0001:0001:0001/16` (where host bits are
    set) and excluding `123:4567:89ab:cdef:123:4567:89ab:cdef`.
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "0123:0001:0001:0001:0001:0001:0001:0001/16",
            "--exclude",
            "123:4567:89ab:cdef:123:4567:89ab:cdef",
        ],
    )
    # print(result.output)
    assert result.exit_code == 1


def test_overlapping_ipv4_error() -> None:
    """
    Test netie with the following `-i` or `--include` overlapping IPs `10.0.0.0/8, 10.1.0.0/16`.
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8, 10.1.0.0/16", "--exclude", "10.0.0.1"])
    # print(result.output)
    assert result.exit_code == 1


def test_overlapping_ipv6_error() -> None:
    """
    Test netie with the following poorly formated IP `10.0.0.999`.
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "0123:4567:89ab:cdef:0123:4567:0:0/96, 0123:4567:89ab:cdef:0123:4567:89ab:0/112",
            "--exclude",
            "0123:4567:89ab:cdef:0123:4567:89ab:cdef",
        ],
    )
    # print(result.output)
    assert result.exit_code == 1


def test_double_ipv4_error() -> None:
    """
    Test netie with the IP `10.0.0.0/8` in the inclusion list (the one passed with the `-i` or
    `--include` option) and the exclusion list (the one passed with the `-e` or `--exclude`
    option).
    """
    result = runner.invoke(main, ["--include", "10.0.0.0/8 ", "--exclude", "10.0.0.0/8"])
    # print(result.output)
    assert result.exit_code == 1


def test_double_ipv6_error() -> None:
    """
    Test netie with the IP `0123:4567:89ab:cdef:0123:4567:89ab:0/112` in the inclusion list (the
    one passed with the `-i` or `--include` option) and the exclusion list (the one passed with the
    `-e` or `--exclude` option).
    """
    result = runner.invoke(
        main,
        [
            "--include",
            "0123:4567:89ab:cdef:0123:4567:89ab:0/112",
            "--exclude",
            "0123:4567:89ab:cdef:0123:4567:89ab:0/112",
        ],
    )
    # print(result.output)
    assert result.exit_code == 1
