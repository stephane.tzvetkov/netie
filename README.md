# NetIE

NetIE ("Network Include / Exclude manager") is a little command-line-interface (cli) tool that has
been thought to help you configure the WireGuard's `AllowedIPs` field, e.g. like described [here](
https://web.archive.org/web/20210705213722/https://www.procustodibus.com/blog/2021/03/wireguard-allowedips-calculator/)


---
## Devlopper guide

### Tests

Run all tests:
```console
$ poetry run pytest -v -r a --black --pylint --flake8 --flakes --pycodestyle --pydocstyle --mypy
$ poetry run pylama ./netie ./tests
$ poetry run vulture ./netie ./tests
```

Check tests coverage:
```console
$ poetry run pytest --cov=netie --cov-report html
$ firefox htmlcov/index.html
```

**TODO**: also check code coverage is 100%

### Hooks

**Optionnal**

Auto-check that everything is fine before pushing:

* see [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) and `.git/hooks`
  examples

**TODO**

---
## License

The [license used for this project](./LICENSE.md) is the [AGPL
license](https://www.gnu.org/licenses/agpl-3.0.en.html).

